Simple Bank project
===================
This project is refactored from https://github.com/techschool/simplebank
* Contact: phupt26@gmail.com

Pre-requisites
==============
1. Install Migrate: https://github.com/golang-migrate/migrate
2. Install SQLC: https://github.com/kyleconroy/sqlc